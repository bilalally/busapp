from flask import Flask, render_template


import urllib as u
import json


app = Flask(__name__)

origValues = {"app_id": "da37cf16", "app_key": "807a578ef9cf158949fd726d5e7311d3", "group": "route", "limit": "", "nextbuses": "yes"}
domain = "https://transportapi.com/v3/uk/"
stop = domain+"bus/stop/450013195/"  #using for the req call
route = "bus/route/"


def busData():
    values = {}
    values["app_id"]=origValues["app_id"]
    values["app_key"] = origValues["app_key"]
    values["group"] = origValues["group"]
    values["limit"] = origValues["limit"]
    values["nextbuses"] = origValues["nextbuses"]  #creating the components of the req url
    # noinspection PyUnresolvedReferences
    params = u.urlencode(values)


    api = stop+"live.json?"
    # noinspection PyUnresolvedReferences
    url = u.urlopen(api+params).read()
    result = json.loads(url)

    return result


def getDepartures():
    dep_97 = {}
    dep_34 = {}

    result = busData()

    #retrieving times of departures + direction

    #bus line 34 departure times

    dep_34['best_departure_estimate']=result["departures"][0]["34"]  #this should return the first time in HH:MM format
    h, m = dep_34.split(':')
    dep_34_time = str(int(h)) + str(int(m)) #this is converting HH:MM -> HHMM, this integer will be compared with the other line to see which time is earlier



    dir_34 = "Horsforth"



    dep_97['best_departure_estimate']=result["departures"][0]["97"]

    h, m = dep_97.split(':')
    dep_97_time = str(int(h)) + str(int(m))



    dir_97 = "Guiseley Morrisons"



    if dep_34_time < dep_97_time:
        currentbus = dep_34
        currentdirection = dir_34
    else:
        currentbus = dep_97
        currentdirection = dir_97

    return currentbus, currentdirection









#-----------------------------------------
@app.route("/")
def home(currentbus, currentdirection):

    getDepartures()
    #print result


    return render_template('home.html',currentbus=currentbus,currentdirection=currentdirection)





@app.route("/timetable") #2nd page
def timetable():

    return "<h1>Timetable</h1>"

if __name__ == '__main__':
    app.run(debug=True) #puts code into debug mode